import subprocess

tests = [
    'aaaaaaaaaaaaaa',
    'badass*',
    '123454545',
    'd'*255,
    'sword'*256,
    'first word',
    'first',
    'second word',
    'third word',
]

notFoundErr = "The key you are looking for was not found!\n"
overflowErr = "The input key is too large! (max size - 255 symb)\n"

expected = [
    notFoundErr,
    notFoundErr,
    notFoundErr,
    notFoundErr,
    overflowErr,
    'word\n',
    'first\n',
    'second word middle one\n',
    'third word last one\n'
]

FAIL = '\033[91m'
OK = '\033[92m'
ENDC = '\033[0m'

print("\n\n ---  Tests started  --- \n")
fileStartCommand = './main'
failures = 0

for i in range(len(tests)):
    print(f"Test {i+1}: ", end='')
    result = subprocess.run([fileStartCommand], input = tests[i].encode('utf_8') + b'\n', capture_output = True)
    if (i <= 4):
       output = result.stderr
    else:
       output = result.stdout

    if (output == expected[i].encode('utf_8')):
       print(OK + 'OK\n' + ENDC)
    else:
       failures += 1
       print(FAIL + 'FAIL\n' + ENDC)



if (failures == 1):
    print(FAIL + f'\n --- {failures} test failed --- \n' + ENDC)
elif(failures > 1):
    print(FAIL + f'\n --- {failures} tests failed --- \n' + ENDC)
else:
    print(OK + f'\n --- All test cases are passed! --- \n' + ENDC)
