%include "dict.inc"
%include "words.inc"
%include "lib.inc"

%define BUFFER_SIZE 256

section .bss

buffer: resb BUFFER_SIZE

section .data

overflow_buffer_message: db "The input key is too large! (max size - 255 symb)", 10, 0
not_found_message: db "The key you are looking for was not found!", 10, 0

section .text

global _start

_start:
    mov rdi, buffer
    mov rsi, BUFFER_SIZE
    call read_line
    test rax, rax
    je .overflow

    mov rdi, rax
    mov rsi, first_el
    call find_word

    test rax, rax
    je .not_found

    mov r12, rax
    lea rdi, [rax + 8]
    call string_length

    xor rdi, rdi
    lea rdi, [r12 + 8]
    lea rdi, [rdi + rax]
    inc rdi
    call print_string
    call print_newline

    jmp .end
.overflow:
    mov rdi, overflow_buffer_message
    call print_error
    jmp .end
.not_found:
    mov rdi, not_found_message
    call print_error
.end:
    call exit
