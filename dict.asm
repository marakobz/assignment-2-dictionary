%include "lib.inc"

global find_word

find_word:
    
    mov r12, rdi
    mov r13, rsi
.loop:
    test r13, r13
    je .not_found

    mov rdi, r12
    lea rsi, [r13 + 8]
    call string_equals
    test rax, rax
    je .continue
    mov rax, r13
    jmp .end
.continue:
    mov r13, [r13]
    jmp .loop
.not_found:
    xor rax, rax
.end:
    ret

